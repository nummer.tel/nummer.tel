function getEnv(name: string) {
  const value = Deno.env.get(name);
  if (!value) {
    throw new Error(`Undefined env var ${name}`);
  }
  return value;
}

const CI_API_V4_URL = getEnv("CI_API_V4_URL");
const CI_PROJECT_ID = getEnv("CI_PROJECT_ID");
const CI_JOB_TOKEN = getEnv("CI_JOB_TOKEN");

const packagesEndpoint = `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages`;

const headers = {
  "JOB-TOKEN": CI_JOB_TOKEN,
};
const allPackagesResponse = await fetch(packagesEndpoint, { headers });
if (allPackagesResponse.status !== 200) {
    throw new Error("Failed to get package list");
}
const allPackages = await allPackagesResponse.json();

const now = Date.now();

for (const pkg of allPackages) {
  if (typeof pkg.created_at !== "string") {
    console.error("no created_at string", pkg);
    continue;
  }
  const createdAt = new Date(pkg.created_at);
  const daysSinceCreated = (now - createdAt.getTime()) / 3_600_000 / 24;
  if (daysSinceCreated < 29) {
    continue;
  }
  const pkgId = pkg.id;
  const pkgVersion = pkg.version;

  console.log(`Going to remove old package ${pkgId} v${pkgVersion}`);
  const deleteResponse = await fetch(`${packagesEndpoint}/${pkgId}`, { method: "DELETE", headers});
  if (deleteResponse.status !== 204) {
    const message = await deleteResponse.text();
    throw new Error(`Failed to delete package ${pkgId} v${pkgVersion}, HTTP ${deleteResponse.status} ${message}`);
  }
}
