import React, { FormEvent, useState } from "react";
import { SearchResultRow, searchZip } from "../lib/searchApi.ts";
import * as Sentry from "sentry/browser";

export default function ZipSearch(props: { sortName?: boolean }) {
  const sortName = props.sortName || false;
  const [results, setResults] = useState([] as SearchResultRow[]);
  const [statusMessage, setStatusMessage] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  return (
    <div className="zip-search">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          minLength={4}
          maxLength={8}
          size={6}
          placeholder="1234AB"
          autoComplete="off"
          autoFocus={true}
        />
        <input type="submit" value="Zoeken" />
      </form>
      <p>{statusMessage}</p>
      <ul className="search-results">{results.map((row) => renderRow(row))}</ul>
    </div>
  );

  function renderRow(row: SearchResultRow) {
    switch (row.rowType) {
      case "Listing":
        return (
          <li key={row.listingId}>
            <a href={row.url}>
              <span className="search-result-url">{row.url}</span>
              <span className="search-result-description">
                {sortName ? row.name : row.address}
              </span>
            </a>
          </li>
        );
      case "ZipHeader":
        return (
          <li key={row.zipHeader} className="zip-header">
            {row.zipHeader}
          </li>
        );
    }
  }

  async function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setResults([]);
    try {
      Sentry.setContext("search", {
        searchTerm,
      });
      setStatusMessage("Laden...");
      const results = await searchZip(searchTerm, sortName);
      if (results === null) {
        setStatusMessage("Ongeldige postcode");
        return;
      }
      setStatusMessage(`‘${searchTerm}’, ${results.count} zoekresultaten`);
      setResults(results.rows);
    } catch (e) {
      console.warn(e);
      setStatusMessage(`Foutmelding: ${e}`);
      Sentry.captureException(e);
    }
  }
}
