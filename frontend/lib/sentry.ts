import * as Sentry from "sentry/browser";
// TODO: enable react integrations
// import * as Sentry from "sentry/react";
import { Integrations } from "sentry/tracing";

declare global {
  interface Window { __APP_META__: {version: string}; }
}

Sentry.init({
  dsn: "https://93d36ddc71544f8c8af128ab2e1abd16@o311805.ingest.sentry.io/1780401",

  release: `nummer-tel@${window.__APP_META__.version}`,
  integrations: [new Integrations.BrowserTracing()],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: .07,
});
