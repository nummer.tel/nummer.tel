import { assertEquals } from "https://deno.land/std@0.115.1/testing/asserts.ts";
import { readVarInt } from "./searchApi.ts";

Deno.test("read 1 byte var int", () => {
  const arr = new Uint8Array([0xa, 0xb, 0xc, 0xd]);
  const actual = readVarInt(new DataView(arr.buffer));
  assertEquals(actual, [1, 10]);
});

Deno.test("read 4 byte var int", () => {
  const arr = new Uint8Array([0xff, 0x12, 0x34, 0x56, 0x78]);
  const actual = readVarInt(new DataView(arr.buffer));
  // little endian
  assertEquals(actual, [4, 0x563412]);
});
