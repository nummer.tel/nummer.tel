const utf8decoder = new TextDecoder("utf-8", { fatal: true });

export type SearchResultRow = ListingRow | ZipHeaderRow;

// Header starting a list of rows with a common ZIP code
interface ZipHeaderRow {
  rowType: "ZipHeader";
  zipHeader: string;
}

interface ListingRow {
  rowType: "Listing";
  listingId: number;
  name: string;
  address: string;
  url: string;
  zip?: string;
}

export async function searchZip(
  zipCode: string,
  sortName: boolean
): Promise<{ rows: SearchResultRow[]; count: number } | null> {
  zipCode = zipCode.toUpperCase().replace(/\s/g, "");
  // match either a full postcode or only the 'postcodegebied' (first 4 numbers)
  if (!/^[1-9][0-9]{3}(?:[A-Z]{2})?$/.test(zipCode)) {
    return null;
  }
  const buffer = await fetchBinaryTable(zipCode);
  if (buffer === null) {
    return { rows: [], count: 0 };
  }
  return decodeRows(buffer, sortName);
}

// only returns the header data map if searchSuffix is absent
async function fetchBinaryTable(
  zipCode: string
): Promise<null | [Uint8Array, Map<number, string>?]> {
  const groupId = zipCode.substring(0, 4);
  const searchSuffix = zipCode.substring(4, 4 + 2);
  // searchSuffix has either length 0 or 2
  const headerData = new Map();

  const response = await fetch(`/api/v1/zip/${groupId}`);
  if (response.status === 404) {
    return null;
  }
  if (response.status !== 200 || response.body === null) {
    throw new Error(`API v1 ${response.status}`, { cause: response as unknown as Error });
  }
  const reader = response.body.getReader();
  let buffer = new Uint8Array(0);
  let zipCount = 0;
  let decodedHeader = false;
  let searchOffset: number | null = null;
  let nextOffset: number | null = null;
  // console.debug('start reading')
  function decodeHeader() {
    const zipCountBytes = 4;
    const headerEntryBytes = 6;
    if (zipCount === 0) {
      if (buffer.length < zipCountBytes) {
        return;
      }
      zipCount = new DataView(buffer.buffer).getUint32(0, true);
      // console.debug('zipCount', zipCount)
    }
    const headerLength = zipCountBytes + zipCount * headerEntryBytes;
    if (buffer.length < headerLength) {
      return;
    }
    for (let zipIndex = 0; zipIndex < zipCount; zipIndex++) {
      const entry = zipCountBytes + zipIndex * headerEntryBytes;
      const suffix = utf8decoder.decode(new DataView(buffer.buffer, entry, 2));
      const entryOffset = new DataView(buffer.buffer, entry + 2).getUint32(
        0,
        true
      );
      if (searchSuffix === "") {
        headerData.set(entryOffset, groupId + suffix);
        continue;
      }
      if (suffix === searchSuffix) {
        searchOffset = entryOffset;
        // console.debug('search offset', searchOffset)
      } else if (searchOffset !== null) {
        // the next offset is needed to know when to stop reading
        nextOffset = entryOffset;
        // searchSize = nextOffset - searchOffset
        break;
      }
    }
    decodedHeader = true;
    // console.debug('header slice ', buffer.length)
    // slice the buffer to the remaining non-header part
    buffer = buffer.slice(headerLength);
  }
  await reader
    .read()
    .then(function processText({ done, value }): Promise<any> | undefined {
      if (done || value === undefined) {
        return;
      }
      const prevBuffer = buffer;
      buffer = new Uint8Array(prevBuffer.length + value.length);
      buffer.set(prevBuffer);
      buffer.set(value, prevBuffer.length);
      if (!decodedHeader) {
        decodeHeader();
      }
      if (nextOffset !== null && buffer.length >= nextOffset) {
        // console.debug('cancel reader')
        reader.cancel("already reached offset for this search");
        return;
      }
      // keep reading
      return reader.read().then(processText);
    });
  // handle empty suffix
  if (searchSuffix === "") {
    return [buffer, headerData];
  }
  // validate length and slice
  if (searchOffset === null) {
    // not found
    return null;
  }
  if (nextOffset === null) {
    console.debug(`buffer len ${buffer.length}`);
    if (buffer.length > searchOffset) {
      return [buffer.slice(searchOffset)];
    }
  } else if (buffer.length >= nextOffset) {
    return [buffer.slice(searchOffset, nextOffset)];
  }
  throw new Error("buffer does not include searchOffset");
}

function decodeRows(
  [arr, headerData]: [Uint8Array, Map<number, string>?],
  sortName: boolean
): {
  rows: SearchResultRow[];
  count: number;
} {
  const rows: SearchResultRow[] = [];
  const state = { buffer: arr.buffer, offset: 0 };
  let count = 0;
  while (state.offset < arr.length) {
    const currentZip = headerData?.get(state.offset);
    if (currentZip && !sortName) {
      rows.push({ rowType: "ZipHeader", zipHeader: currentZip });
    }
    const listingId = new DataView(state.buffer, state.offset, 4).getUint32(
      0,
      true
    );
    state.offset += 4;
    const name = readString(state);
    const address = readString(state);
    const url = deriveUrl(name, listingId);
    if ([52338251].includes(listingId)) {
      continue;
    }
    count += 1;
    rows.push({
      rowType: "Listing",
      listingId,
      name,
      address,
      url,
      zip: currentZip,
    });
  }
  if (sortName) {
    rows.sort((a, b) => {
      if (a.rowType !== "Listing" || b.rowType !== "Listing") {
        console.error(a, b);
        throw new Error("unexpected row type");
      }
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
  }
  return {
    rows,
    count,
  };
}

function readString(state: { buffer: ArrayBuffer; offset: number }) {
  const [bytesRead, len] = readVarInt(
    new DataView(state.buffer, state.offset, 4)
  );
  state.offset += bytesRead;
  const str = utf8decoder.decode(new DataView(state.buffer, state.offset, len));
  state.offset += len;
  return str;
}

/**
 * variable length integer algorithm:
 * if first byte is 0xff, interpret the next 3 bytes as uint24 (little endian),
 * otherwise interpret the single byte as uint8
 */
export function readVarInt(view: DataView) {
  const u8 = view.getUint8(0);
  if (u8 === 0xff) {
    console.debug("large var int");
    return [4, view.getUint32(0, true) >> 8];
  } else {
    return [1, u8];
  }
}

function deriveUrl(name: string, listingId: number) {
  // should not end with '-', no double '-', replace al non-alphanumeric chars with '-', normalize accents
  // PostgreSQL way: trim(both '-' from regexp_replace(lower(unaccent(data->>'name')), '[-]*[^a-z0-9]+[-]*', '-', 'g'))
  const normalized = name
    .normalize("NFD")
    .replace(/\p{Diacritic}/gu, "")
    .toLowerCase()
    .replaceAll(/[-]*[^a-z0-9]+[-]*/g, "-")
    .match(/^-*(.*?)-*$/)![1];
  return `https://www.detelefoongids.nl/${normalized}/wp${listingId}/9-1/`;
}
