# Nummer.tel Frontend

Tools used:

- Web Application Framework: <https://alephjs.org/>
- JavaScript Framework: React

## Updating the lock file

```sh
deno cache --no-check --lock=lock.json --lock-write --import-map=./import_map.json ./lib/*.ts app.tsx *.ts */*.tsx https://deno.land/x/aleph@v0.3.0-beta.19/cli.ts https://deno.land/x/aleph@v0.3.0-beta.19/install.ts
```
