import React from "react";
import ZipSearch from "~/components/zip-search.tsx";

export default function Home() {
  return (
    <main>
      <head>
        <title>Nummer.tel — Zoek in de telefoongids op postcode</title>
        <meta property="og:title" content="Nummer.tel — Zoek in de telefoongids op postcode"/>
        <meta
          name="description"
          content="Zoekmachine voor personen die in een bepaalde straat wonen, die doorverwijst naar www.detelefoongids.nl voor contactgegevens. Vind nummers via een postcode."
        />
      </head>
      <p>
        Dit is een zoekmachine om adresgegevens op www.detelefoongids.nl te
        vinden. Het werkt alleen met de witte pagina’s, dus geen bedrijven. De
        zoekresultaten zijn hyperlinks naar de website van het telefoonboek
        zodat je daar contactgegevens van personen kan opzoeken.
      </p>
      <h1>Zoek in de telefoongids op postcode</h1>
      <ZipSearch />
      <br />
      <hr />
      <nav>
        <p>
          <a href="/sorteer-naam/">Zoek met sortering op achternaam</a>
        </p>
        <p>
          <a href="/over/">Informatie, privacyverklaring &amp; contact</a>
        </p>
      </nav>
    </main>
  );
}
