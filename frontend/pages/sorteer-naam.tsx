import React from "react";
import ZipSearch from "~/components/zip-search.tsx";

export default function SortName() {
  return (
    <main>
      <head>
        <title>Nummer.tel — Sortering op achternaam</title>
        <meta property="og:title" content="Sortering op achternaam, zoek adres in de telefoongids"/>
        <meta
          name="description"
          content="Zoek hiermee naar postcodes in www.detelefoongids.nl, en krijg resultaten die gesorteerd zijn op achternaam."
        />
      </head>
      <h1>Zoek naar postcode met sortering op achternaam</h1>
      <p>
        Net als de startpagina zoekt deze functie een postcode in www.detelefoongids.nl,
        maar hier sorteert die de resultaten op achternaam in alfabetische volgorde.
        Je kan ook een heel postcodegebied doorzoeken door alleen de eerste vier cijfers van een postcode
        in te typen.
      </p>
      <ZipSearch sortName={true} />
      <br />
      <hr />
      <nav>
        <p>
          <a href="/">Terug naar de startpagina</a>
        </p>
      </nav>
    </main>
  );
}
