import React from "react";

export default function About() {
  return (
    <main>
      <head>
        <title>Nummer.tel — Over deze webapp</title>
        <meta property="og:title" content="Informatie over de Nummer.tel webapp"/>
        <meta
          name="description"
          content="Lees hier het privacybeleid voor persoonsgegevens, de regels voor zoekmachines, en hoe contact op te nemen met het ontwikkelteam."
        />
      </head>
      <h1>Informatie over deze webapp</h1>
      <p>Lees hier onder meer hoe Nummer.tel omgaat met persoonsgegevens.</p>
      <h2>Vergeetrecht</h2>
      <p>
        Als u uw gegevens wilt wijzigen of verwijderen uit het telefoonboek, ga
        dan naar {}
        <a href="https://mijnnummervermelding.nl/">
          mijnnummervermelding.nl
        </a>{" "}
        {}
        (zie ook{" "}
        <a href="https://www.detelefoongids.nl/static/veel_gestelde_vragen">
          www.detelefoongids.nl/static/veel_gestelde_vragen
        </a>
        ). Het kan even duren voordat de wijzigingen ook in zoekmachines
        zichtbaar zijn. Wilt u het proces versnellen voor Nummer.tel, dien dan
        een verzoek in via het emailadres onderaan deze pagina, want alle
        Nederlanders hebben het ‘recht om vergeten te worden’.
      </p>
      <h2>Privacyverklaring</h2>
      <p>
        Deze website gebruikt geen cookies en slaat niet op naar welke postcodes
        je zoekt. Hier volgt een uitleg wat voor gegevens Nummer.tel wel
        verzamelt van bezoekers.
      </p>
      <p>
        <a href="https://www.cloudflare.com/web-analytics/">
          Cloudflare Web Analytics
        </a>{" "}
        {}
        telt het aantal bezoekers van deze site en meet hoe snel de pagina's
        laden. Die gegevens zijn ingedeeld per land, browser, besturingssysteem,
        referer, en apparaattype (desktop, mobiel, tablet).
      </p>
      <p>
        Nummer.tel gebruikt de dienst ‘Sentry’ om fouten op te sporen en te
        controleren of de app goed werkt op alle soorten apparaten. Als de app
        geen problemen tegenkomt, verstuurt die software-library een rapport met
        de versienummers van de app, de webbrowser, en het besturingssysteem.
        Maar als er foutmeldingen voorkomen verstuurt die uitgebreidere
        rapporten met onder andere ook het tijdstip van de error, het IP-adres,
        en informatie over wat er van tevoren gebeurde in de app. Sentry's
        servers verwerken deze rapporten en bewaren de gegevens. Lees hier hun
        privacybeleid:{" "}
        <a href="https://sentry.io/privacy/">https://sentry.io/privacy/</a>.
      </p>
      <h2>Database</h2>
      <p>
        Deze website heeft andere beheerders dan detelefoongids.nl, het is
        geen officieel project van Youvia.
        Nummer.tel vult zijn database met een eigen ‘web crawler’ net als andere zoekmachines dat kunnen,
        en volgt netjes de regels.
      </p>
      <p>
        Google en Microsoft Bing maken ook postcodes in de telefoongids doorzoekbaar,
        maar ze vinden niet alles.
        Voor de zoekopdracht {}
        <a href="https://www.google.com/search?q=%225043LW%22+site%3Adetelefoongids.nl">
          "5043LW" site:detelefoongids.nl
        </a>{" "}
        geeft Google bijvoorbeeld 2 resultaten. Dezelfde{" "}
        <a href="https://www.bing.com/search?q=%225043LW%22+site%3adetelefoongids.nl">
          zoekopdracht op Bing
        </a>{" "}
        geeft 445 resultaten, waarvan er veel dubbel zijn (op het moment van schrijven september 2021).
        Nummer.tel lost die problemen op door duplicaten te
        verwijderen, en geeft voor die postcode 105 resultaten.
      </p>
      <p>
        In theorie zou je in drie richtingen kunnen zoeken: op naam, op adres, of op telefoonnummer.
        Die laatste richting – van telefoonnummer naar contactgegevens – is niet de bedoeling.
        In de {}
        <a href="https://www.detelefoongids.nl/static/Privacy_verklaring">
          policy van detelefoongids.nl
        </a>{" "}
        {}
        staat namelijk {" "}
        <i>
          ”Het is niet mogelijk om aan de hand van een
          telefoonnummer, een naam te vinden”
        </i>
        . De database van Nummer.tel bevat geen telefoonnummers en kan dus niet een
        omgekeerde zoekfunctie mogelijk maken.
      </p>
      <p>
        Een van de ‘technische beveiligingsmaatregelen’ die Youvia heeft genomen
        staat zoekmachines niet toe om telefoonnummers doorzoekbaar te maken,
        maar dat geldt niet voor adresgegevens (waaronder postcodes).
        Zoekmachines volgen namelijk de regels in{" "}
        <a href="https://www.detelefoongids.nl/robots.txt">www.detelefoongids.nl/robots.txt</a>
        . Je ziet daar bijvoorbeeld regels voor ‘GoogleBot’ en ‘msnbot’ (Bing).
        De regel “Disallow: /*9-1/” zorgt ervoor dat zoekmachines de pagina's overslaan waar telefoonnummers
        van personen zichtbaar zijn.
        Maar er staan niet zulke regels voor ‘5-1’ en ‘7-1’,
        dat betekent dat zoekmachines pagina's met alleen namen en adressen wel mogen indexeren.
        Daarom kan je via zoekmachines wel de de telefoongids doorzoeken op naam of adres, maar
        niet op telefoonnummers.
      </p>
      <h2>Ontwikkelteam</h2>
      Nummer.tel heeft maar één softwareontwikkelaar (die liever niet zijn naam
      hier noemt). Hij bouwt dit als hobbyproject.
      Er zijn nog verschillende verbeteringen mogelijk, zoals
      ondersteuning voor oude browsers.
      Op lange termijn komt er misschien een functie om ook gele pagina's te
      indexeren.
      <h2>Contact</h2>
      <p>
        Je kan contact opnemen via het emailadres info@nummer.tel, of open een{" "}
        <a href="https://gitlab.com/nummer.tel/nummer.tel/-/issues/new">
          GitLab issue
        </a>{" "}
        {}
        voor technische zaken.
      </p>
      <br />
      <hr />
      <nav>
        <p>
          <a href="/">Terug naar de startpagina</a>
        </p>
      </nav>
    </main>
  );
}
