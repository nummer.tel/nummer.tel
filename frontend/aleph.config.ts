import { Aleph, Config, Plugin } from "aleph/types";
import { join } from "https://deno.land/std@0.115.1/path/mod.ts";

const appMeta = JSON.stringify({
  version: await gitCommit(),
});

async function gitCommit() {
  const commitSha = Deno.env.get("CI_COMMIT_SHA");
  if (commitSha && commitSha.length === 40) {
    return commitSha;
  }
  const p = Deno.run({
    cmd: ["git", "rev-parse", "HEAD"],
    stdout: "piped",
  });
  const { code } = await p.status();
  if (code !== 0) {
    throw new Error("git rev-parse failed");
  }
  const rawOutput = await p.output();
  return new TextDecoder().decode(rawOutput);
}

const sentryPlugin: Plugin = {
  name: "sentry",
  setup: (aleph) => {
    if (aleph.mode === "production") {
      let fileName = "";
      aleph.onRender(async ({ html }) => {
        if (!fileName) {
          fileName = await findSentryBundle(aleph);
          // console.info('[Sentry Plugin] bundle file ', fileName)
        }
        // console.info('[Sentry Plugin] scripts', html.scripts)
        const inlineIndex = html.scripts.findIndex(
          (f) => typeof f === "string" && f.startsWith("window.__ALEPH__=")
        );
        const inlineCode = `${html.scripts[inlineIndex]};window.__APP_META__=${appMeta};`;
        html.scripts.splice(inlineIndex, 1, inlineCode, {
          src: `/_aleph/${fileName}`,
          type: "module",
        });
      });
    }
  },
};

async function findSentryBundle(aleph: Aleph) {
  const buildDir = (aleph as unknown as { buildDir: string }).buildDir;
  for await (const f of Deno.readDir(join(buildDir, "lib"))) {
    if (f.name.startsWith("sentry.bundle.")) {
      return "lib/" + f.name;
    }
  }
  throw new Error("sentry bundle not found");
}

export default <Config>{
  plugins: [sentryPlugin],
  i18n: {
    defaultLocale: "nl",
  },
};
