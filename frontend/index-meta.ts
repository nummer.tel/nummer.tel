import { parse } from "https://deno.land/std@0.122.0/flags/mod.ts";

const args = parse(Deno.args, {
  string: ["version", "sha256"],
});
console.dir(args);

const indexVersion = args["version"];
if (!indexVersion) {
  throw new Error("requires version arg");
}
const tarSha256 = args["sha256"];
if (!tarSha256) {
  throw new Error("requires sha256 arg");
}

const data = JSON.stringify({ indexVersion, tarSha256 });
await Deno.writeTextFile("dist/api/v1/index-meta.json", data);
