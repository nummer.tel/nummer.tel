import React, { ComponentType } from "react";
import { useDeno, useRouter } from "aleph/framework/react/mod.ts";

// Only here to add to dependency graph for dynamic bundling
// Don't actually call this.
export function _sentryBundleDoNotCall() {
  import("./lib/sentry.ts");
}

export default function App({
  Page,
  pageProps,
}: {
  Page: ComponentType<any>;
  pageProps: any;
}) {
  const currentYear = useDeno(() => {
    return new Date().getUTCFullYear();
  });
  let { pathname } = useRouter();
  if (pathname.length > 2 && !pathname.endsWith("/")) {
    pathname += "/";
  }

  return (
    <>
      <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="./style/index.css" />
        {/* Generated with https://realfavicongenerator.net/ */}
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#2b5797" />
        <meta name="theme-color" content="#5bbad5" />

        <meta property="og:type" content="website" />
        <meta property="og:url" content={"https://www.nummer.tel" + pathname} />
        <meta property="og:image" content="/nummer-tel-logo.png" />
      </head>
      <header>
        <div className="wrapper">
          <a href="/">Nummer.tel</a>
        </div>
      </header>
      <aside className="wrapper">
        <p className="notice">
          Sinds 20 december zijn links naar www.detelefoongids.nl niet meer geldig omdat de website is verhuisd naar
          www.goudengids.nl. Het kan even duren voordat deze zoekmachine weer functioneert.
        </p>
      </aside>
      <Page {...pageProps} />
      <footer>
        <div>Copyright &copy;{currentYear}&nbsp;&nbsp;</div>
        <div>
          <a href="https://gitlab.com/nummer.tel/nummer.tel/-/blob/main/LICENSE">
            Source code licensed under GNU GPLv3
          </a>
        </div>
      </footer>
    </>
  );
}
