package erasure

import (
	"encoding/json"
	"testing"
)

func TestHashing(t *testing.T) {
	inputJson := `{ "index": 8, "name": "Hello", "address": { "city": { "name": "World" } }, "_links": {} }`
	if !IsOnErasureList(json.RawMessage(inputJson)) {
		t.Fatal("Expected to be present")
	}
	inputJson = `{ "index": 8, "name": "Hi", "address": { "city": { "name": "World" } }, "_links": {} }`
	if IsOnErasureList(json.RawMessage(inputJson)) {
		t.Fatal("Expected to be absent")
	}
	inputJson = `{ "index": 8, "name": "Hello", "address": { "city": { "name": "There" } }, "_links": {} }`
	if IsOnErasureList(json.RawMessage(inputJson)) {
		t.Fatal("Expected to be absent")
	}
}
