package erasure

import (
	"bytes"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
)

var erasureList = []string{
	"7a70be0fed8cbaf524b44d321a1c56c8ee084fe423bfc8e5a470e402f9c242fffba5931eb50f9977fdb6edc25b8553cb",
	"d011be7bb78aabb9f5632634069142bf80b72602ec81c88d7fa88339361d056708ba3e9102d44157cf48f6d8acfbe9df",
	"67254f3153ba4c19644930ab95808634f1532f5a860c0dda3c1626f130d85a970838ff207842307baa1beec86cc2afef",
}

var erasureHashSet = map[[48]byte]struct{}{}

func init() {
	for _, hash := range erasureList {
		hashSlice, err := hex.DecodeString(hash)
		if err != nil {
			panic("Incorrect hash " + hash)
		}
		hashArr := *(*[48]byte)(hashSlice)
		erasureHashSet[hashArr] = struct{}{}
	}
}

func IsOnErasureList(rawRecord json.RawMessage) bool {
	var record struct {
		Name    string `json:"name"`
		Address struct {
			City struct {
				Name string `json:"name"`
			} `json:"city"`
		} `json:"address"`
	}

	err := json.Unmarshal(rawRecord, &record)
	if err != nil {
		return false
	}

	var buf bytes.Buffer
	buf.Write([]byte(record.Name))
	buf.WriteByte(0)
	buf.Write([]byte(record.Address.City.Name))

	hash := sha512.Sum384(buf.Bytes())

	_, exists := erasureHashSet[hash]

	return exists
}
