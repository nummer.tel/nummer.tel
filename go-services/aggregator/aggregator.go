package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jackc/pgx/v4"
)

var scanId = 0
var targetDir string

func main() {
	var err error
	scanId, err = strconv.Atoi(os.Args[1])
	if err != nil {
		log.Panicln("first argument must be integer for scan_id ", err)
	}
	targetDir, err = filepath.Abs(os.Args[2])
	if err != nil {
		log.Panicln("second argument must be path ", err)
	}
	log.Println("info, start aggregate")
	aggregate()
	log.Println("info, exit")
}

type State struct {
	db *pgx.Conn
}

func aggregate() {
	db, err := pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Printf("Unable to connect to database: %v\n", err)
		return
	}
	defer db.Close(context.Background())

	s := State{
		db,
	}
	s.query()

	log.Println("scanId ", scanId)
}

func (s *State) query() {
	rows, err := s.db.Query(context.TODO(), `select id, zip, name,
    street || ' ' || house_no || coalesce('/' || house_letter, '') || coalesce(', ' || city, '') as address
    from (
        select id, zip_code,
            data->>'name' as name,
            data->'address'->>'zipCode' as zip,
            data->'address'->>'street' as street,
            data->'address'->>'houseNoFrom' as house_no,
            data->'address'->>'houseLetterFrom' as house_letter,
            data->'address'->'city'->>'name' as city
            from spider_results3
            where scan_id=$1 and zip_code<>0
    ) a
    order by zip_code, street,
		substring(house_no from '[0-9]+')::bigint,
        substring(house_letter from '^[^0-9]+') NULLS FIRST,
        substring(house_letter from '[0-9]+')::bigint NULLS FIRST,
        house_letter NULLS FIRST,
        name;`, scanId)
	if err != nil {
		log.Printf("DB select: %v\n", err)
		return
	}
	defer rows.Close()
	var currentGroup *ZipGroup
	// Iterate through the result set
	for rows.Next() {
		var id int
		var zip string
		var name string
		var address string
		err = rows.Scan(&id, &zip, &name, &address)
		if err != nil {
			log.Printf("DB scan: %v\n", err)
			continue
		}
		groupId := zip[:4]
		zipSuffix := zip[4:6]
		if currentGroup == nil || currentGroup.groupId != groupId {
			currentGroup.writeToFile()
			currentGroup = &ZipGroup{groupId, nil}
		}
		currentGroup.append(zipSuffix, id, name, address)
	}
	currentGroup.writeToFile()
	// Any errors encountered by rows.Next or rows.Scan will be returned here
	if err := rows.Err(); err != nil {
		log.Printf("DB rows: %v\n", err)
		return
	}
}

type ZipGroup struct {
	groupId  string
	zipCodes []ZipBuf
}

type ZipBuf struct {
	suffix string
	buf    bytes.Buffer
}

func (z *ZipGroup) append(zipSuffix string, id int, name, address string) {
	// data is sorted, so only need to check latest zipSuffix
	if len(z.zipCodes) == 0 || z.zipCodes[len(z.zipCodes)-1].suffix != zipSuffix {
		z.zipCodes = append(z.zipCodes, ZipBuf{zipSuffix, bytes.Buffer{}})
	}
	buf := &z.zipCodes[len(z.zipCodes)-1].buf
	err := binary.Write(buf, binary.LittleEndian, uint32(id))
	if err != nil {
		log.Panicf("write int %v", err)
	}
	appendString(buf, name)
	appendString(buf, address)
}

func appendString(buf *bytes.Buffer, str string) {
	// write the string byte count as unsigned integer and then the UTF-8 encoding
	if len(str) >= 1<<24-1 {
		log.Panicf("too long string %s", str)
	}
	// simple variable length integer:
	// if first byte is 0xff, interpret the next 3 bytes as uint24 (little endian),
	// otherwise interpret the single byte as uint8
	var length interface{}
	if len(str) < 0xff {
		length = uint8(len(str))
	} else {
		log.Printf("using 24 bit integer for %s\n", str)
		length = (uint32(len(str)) << 8) | uint32(0xff)
	}
	err := binary.Write(buf, binary.LittleEndian, length)
	if err != nil {
		log.Panicf("write int %v", err)
	}
	_, _ = fmt.Fprint(buf, str)
}

func (z *ZipGroup) writeToFile() {
	if z == nil {
		return
	}
	file, err := os.Create(filepath.Join(targetDir, fmt.Sprintf("%s.bin", z.groupId)))
	if err != nil {
		log.Printf("Error open file for group %s: %v\n", z.groupId, err)
		return
	}
	defer file.Close()

	var indexBuf bytes.Buffer
	// append count of index entries (each entry is 2 + 4 bytes long)
	binary.Write(&indexBuf, binary.LittleEndian, int32(len(z.zipCodes)))
	// append index entries
	var offset int32
	for _, z := range z.zipCodes {
		fmt.Fprint(&indexBuf, z.suffix)
		binary.Write(&indexBuf, binary.LittleEndian, offset)
		offset += int32(z.buf.Len())
	}
	// write index to file
	_, err = io.Copy(file, &indexBuf)
	if err != nil {
		log.Printf("Error write file index: %v\n", err)
		return
	}
	// write content to file
	for i := range z.zipCodes {
		_, err = io.Copy(file, &z.zipCodes[i].buf)
		if err != nil {
			log.Printf("Error write file data: %v\n", err)
			return
		}
	}
	// log.Printf("wrote file %s\n", z.groupId)
}
