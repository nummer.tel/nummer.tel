package main

import (
	"bytes"
	"context"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/jackc/pgx/v4"
)

func TestPathSplit(t *testing.T) {
	url, err := url.Parse("https://www.detelefoongids.nl/vd-ruyt/emmen/7-1/")
	if err != nil {
		t.Error(err)
	}
	pathParts := strings.Split(url.Path, "/")
	if l := len(pathParts); l != 5 {
		t.Error("len ", l)
	}
	if pathParts[1] != "vd-ruyt" {
		t.Error("path part ", pathParts[1])
	}
	if pathParts[2] != "emmen" {
		t.Error("path part ", pathParts[2])
	}
	if pathParts[3] != "7-1" {
		t.Error("path part ", pathParts[3])
	}
}

func TestPathSplit8(t *testing.T) {
	url, err := url.Parse("https://www.detelefoongids.nl/schippers/8-1/")
	if err != nil {
		t.Error(err)
	}
	pathParts := strings.Split(url.Path, "/")
	if l := len(pathParts); l != 4 {
		t.Error("len ", l)
	}
	if pathParts[1] != "schippers" {
		t.Error("path part ", pathParts[1])
	}
	if pathParts[2] != "8-1" {
		t.Error("path part ", pathParts[2])
	}
}

func TestJsonParseAndDbInser(t *testing.T) {
	t.Skip("Skipping test because it requires database")
	scanId = 1
	testFile, err := os.ReadFile("./test-data.json")
	if err != nil {
		t.Fatal(err)
	}
	reader := bytes.NewReader(testFile)
	db, err := pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		t.Fatalf("Unable to connect to database: %v\n", err)
		return
	}
	defer db.Close(context.Background())

	c := Crawler{nil, db, nil, 0}
	c.parseSearchResult(reader, "/my/test/string/", "my-test")
}
