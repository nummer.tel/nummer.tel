package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"context"
	"crypto/tls"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/jackc/pgx/v4"
	"gitlab.com/nummer.tel/nummer.tel/erasure"
)

var scanId = 0

func main() {
	var err error
	scanId, err = strconv.Atoi(os.Args[1])
	if err != nil {
		log.Panicln("error, first argument must be integer for scan_id ", err)
	}
	log.Println("info, start")
	crawl()
	log.Println("info, exit")
}

type Crawler struct {
	httpClient  *http.Client
	db          *pgx.Conn
	personNames []string
	requests    uint32
}

func crawl() {
	dialer := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
	}
	t := &http.Transport{}
	t.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		return dialer.DialContext(ctx, network, addr)
	}
	t.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	db, err := pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Printf("error, Unable to connect to database: %v\n", err)
		return
	}
	defer db.Close(context.Background())

	c := Crawler{
		&http.Client{
			Transport: t,
			Timeout:   30 * time.Second,
		},
		db,
		make([]string, 0, 500_000),
		0,
	}
	start := time.Now()

	defer func() {
		elapsed := time.Since(start)
		log.Printf("warn, statistics: request count %d, duration %s", c.requests, elapsed)
	}()

	if scanId == 0 {
		for i := 1; i < 200; i++ {
			reachedEnd := c.downloadSitemapPart(i)
			if reachedEnd {
				break
			}
		}

		log.Println("info, total name count ", len(c.personNames))
		namesFile, err := os.Create("./sitemap.txt")
		if err != nil {
			log.Printf("error, Create index: %v\n", err)
			return
		}
		sort.Strings(c.personNames)
		for _, search := range c.personNames {
			if strings.Contains(search, "\n") {
				log.Printf("error, Name contains \\n: %s\n", search)
				return
			}
			name, _ := searchToName(search)
			if name == "" {
				log.Printf("error, Empty name: %s\n", search)
				continue
			}
			fmt.Fprintln(namesFile, search)
		}
	} else {
		log.Println("scanId ", scanId)
		namesFile, err := os.Open("./sitemap.txt")
		if err != nil {
			log.Printf("error, Open index: %v\n", err)
			return
		}
		scanner := bufio.NewScanner(namesFile)
		scanner.Split(bufio.ScanLines)
		index := 0
		for scanner.Scan() {
			search := scanner.Text()
			name, city := searchToName(search)
			if name == "-" {
				log.Printf("warn, Search contains dash: %s\n", search)
				continue
			}
			if name == "" {
				log.Printf("error, Invalid search: %s\n", search)
				continue
			}
			if index%1000 == 0 {
				log.Println("info, index ", index, " : ", name)
			}
			c.searchPerson(search, name, city)
			index += 1
		}
	}
}

func (c *Crawler) downloadSitemapPart(page int) (reachedEnd bool) {
	log.Println("info, downloading sitemap page ", page)
	reachedEnd = false
	url := fmt.Sprintf("https://www.detelefoongids.nl/sitemap/nl/xml/personen-in-locatie/personen-in-locatie_sitemap_%d.xml.gz", page)
	resp, err := c.httpClient.Get(url)
	if err != nil {
		log.Printf("error, HTTP: %v\n", err)
		return
	}
	defer resp.Body.Close()

	c.requests += 1

	switch resp.StatusCode {
	case 200:
		// continue
	case 404:
		reachedEnd = true
		return
	default:
		log.Printf("error, HTTP status: %s\n", resp.Status)
		return
	}
	encoder, err := gzip.NewReader(resp.Body)
	if err != nil {
		log.Printf("error, gzip start: %v\n", err)
		return
	}
	var buf bytes.Buffer
	_, err = io.Copy(&buf, encoder)
	if err != nil {
		log.Printf("error, gzip copy: %v\n", err)
		return
	}
	// xml parsing
	c.parseSitemapXml(&buf)
	return
}

func (c *Crawler) parseSitemapXml(b *bytes.Buffer) {
	type XmlUrl struct {
		Loc string `xml:"loc"`
	}
	var sitemap struct {
		Urlset []XmlUrl `xml:"url"`
	}
	decoder := xml.NewDecoder(b)
	err := decoder.Decode(&sitemap)
	if err != nil {
		log.Printf("error, XML decoder: %v\n", err)
		return
	}
	if len(sitemap.Urlset) == 0 {
		log.Printf("error, Zero URLs\n")
		return
	}
	for _, item := range sitemap.Urlset {
		u, err := url.Parse(item.Loc)
		if err != nil {
			log.Printf("error, URL parse %s", item.Loc)
			return
		}
		c.personNames = append(c.personNames, u.Path)
	}
}

func searchToName(search string) (personName string, city string) {
	pathParts := strings.Split(search, "/")
	if len(pathParts) == 4 {
		// '8-1' identifies achternaam
		if pathParts[2] != "8-1" {
			log.Println("error, path[2] expected 8-1 but ", pathParts[2])
			return
		}
		personName = pathParts[1]
	} else if len(pathParts) == 5 {
		// '7-1' identifies stad/achternaam
		if pathParts[3] != "7-1" {
			log.Println("error, path[3] expected 7-1", pathParts[3])
			return
		}
		personName = pathParts[1]
		city = pathParts[2]
		if len(city) == 0 {
			log.Println("error, empty cityName", search)
		}
	} else {
		log.Println("error, unexpected path len ", search)
		return
	}
	if len(personName) == 0 {
		log.Println("error, empty personName", search)
		return
	}
	return
}

func (c *Crawler) searchPerson(search, personName, city string) {
	whereTerm := map[string]string{}
	if city != "" {
		whereTerm["where"] = city
	}

	// the API doesn't support higher limit
	const limit = 150
	startIndex := 1

	for {
		searchParams, err := json.Marshal(map[string]interface{}{
			"originPath": search, // this value doesn't seem to affect the result
			"what":       personName,
			"whereTerm":  whereTerm, // empty JSON object {}
			"collapsing": true,
			"sortBy":     "relevance",
			"limit":      limit,
			"startIndex": startIndex,
		})
		if err != nil {
			log.Printf("error, searchParams marshal for %s: %v\n", search, err)
			return
		}
		resp := c.httpSearch(search, searchParams)
		if resp == nil {
			return
		}
		defer resp.Body.Close()
		totalResultCount, pageResultCount := c.parseSearchResult(resp.Body, search, personName)
		if pageResultCount > limit {
			log.Printf("error, too high pageResultCount %s %d %d %d\n", search, totalResultCount, pageResultCount, startIndex)
		}
		if pageResultCount != totalResultCount-startIndex+1 && pageResultCount != limit {
			log.Printf("error, unexpected pageResultCount %s %d %d %d\n", search, totalResultCount, pageResultCount, startIndex)
		}
		startIndex += limit
		// example limit 25
		// startIndex 26 > totalResultCount 24, break
		// startIndex 26 > totalResultCount 25, break
		// startIndex 26 = totalResultCount 26, continue
		// startIndex 26 < totalResultCount 27, continue
		if startIndex > totalResultCount {
			break
		}
	}
}

func (c *Crawler) httpSearch(search string, searchParams []byte) *http.Response {
	url := "https://www.detelefoongids.nl/v1/rest/api/wpSearchResult"
	for i := 0; i < 200; i++ {
		if i > 0 {
			log.Printf("warn, retrying %s #%d in a few seconds", search, i)
			time.Sleep(time.Duration(1+i*i) * time.Second)
		}
		resp, err := c.httpClient.Post(url, "application/json", bytes.NewBuffer(searchParams))
		if err != nil {
			log.Printf("warn, HTTP search %s: %v\n", search, err)
			continue
		}
		c.requests += 1

		// closing body only if not returning
		if resp.StatusCode != 200 {
			resp.Body.Close()
			log.Printf("warn, HTTP status search %s: %s\n", search, resp.Status)
			continue
		}
		return resp
	}
	log.Printf("error, giving up retrying %s", search)
	return nil
}

func (c *Crawler) parseSearchResult(body io.Reader, search, personName string) (totalResultCount, pageResultCount int) {
	totalResultCount = 0
	var data map[string]json.RawMessage
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&data)
	if err != nil {
		log.Printf("error, JSON parse %s: %s\n", search, err)
		return
	}
	err = json.Unmarshal(data["totalResultCount"], &totalResultCount)
	if err != nil {
		log.Printf("error, JSON parse totalResultCount %s: %s\n", search, err)
		return
	}
	if totalResultCount == 0 {
		log.Printf("debug, totalResultCount == 0 %s\n", search)
		return
	}

	var results []json.RawMessage
	err = json.Unmarshal(data["results"], &results)
	if err != nil {
		log.Printf("error, JSON parse results %s: %s\n", search, err)
		return
	}
	pageResultCount = len(results)
	if pageResultCount > totalResultCount {
		log.Printf("error, JSON %s unexpected len(results) > %d\n", search, pageResultCount)
		return
	}
	for _, result := range results {
		c.parseRow(search, result)
	}
	return
}

func (c *Crawler) parseRow(search string, result json.RawMessage) {
	var info struct {
		ListingId string `json:"listingId"`
		Address   struct {
			ZipCode *string `json:"zipCode"`
		} `json:"address"`
	}

	err := json.Unmarshal(result, &info)
	if err != nil {
		log.Printf("error, JSON parse 2 %s: %s\n", search, err)
		return
	}

	id, err := strconv.Atoi(strings.TrimPrefix(info.ListingId, "WP"))
	if err != nil {
		log.Printf("error, listingId parse %s: %s\n", search, err)
		return
	}

	if erasure.IsOnErasureList(result) {
		log.Printf("warn, Skipping item on right to erasure list %s", info.ListingId)
		return
	}

	var zipCode = 0
	if z := info.Address.ZipCode; z != nil {
		zipCode = convertZip(*z, search)
	}
	_, err = c.db.Exec(context.TODO(),
		`INSERT INTO spider_results3 (id, scan_id, zip_code, data)
			VALUES ($1, $2, $3, $4)
			ON CONFLICT (id, scan_id) DO NOTHING`,
		id, scanId, zipCode, result,
	)
	if err != nil {
		log.Printf("error, DB insert %s: %s\n", search, err)
		return
	}
}

func convertZip(zipCodeS string, search string) int {
	if len(zipCodeS) == 0 {
		return 0
	}
	if zipCodeS == "2841" {
		log.Printf("warn, zipCode parse (ignored) %s: %s\n", search, zipCodeS)
		return 0
	}
	if len(zipCodeS) != 6 {
		log.Printf("error, zipCode parse %s: %s\n", search, zipCodeS)
		return 0
	}
	zipCode, err := strconv.Atoi(zipCodeS[:4])
	if err != nil {
		log.Printf("error, zipCode parse 2 %s: %v\n", search, err)
		return 0
	}
	zipCode *= 1000
	zipCode += 26 * (int(zipCodeS[4]) - 'A')
	zipCode += int(zipCodeS[5]) - 'A'
	return zipCode
}
