# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased

## 0.2.0 - 2022-02-06

### Added

- Privacyverklaring
- Automated running crawler and aggregator with Gitlab CI
- Pagina 'sorteer-naam'

### Changed

- You can now search for postcodegebied instead of only full postcodes
- Improved error handling and logging for crawler

## 0.1.0 - 2021-10-11

### Added

- Publish source code as public Gitlab repository
- Website footer linking to license and repo
- CI/CD for frontend

## 0.0.3 - 2021-09-16

### Added

- Autofocus to search input field

### Changed

- Made the error messages more clear
- Accept spaces in postcode

## 0.0.2 - 2021-09-08

### Added

- Website logo (favicon)

### Changed

- Apply feedback on text

### Fixed

- Parse binary file header correctly

## 0.0.1 - 2021-09-04

First version deployed to nummer.tel

### Added

- A functional search engine frontend
